# %%
#Importação de pacotes
import pandas as pd

# %%
#Definindo uma função para o IMC
def IMC(valor):
    if valor< 18.5:
        obs = "Magreza"
    elif valor>30:
        obs = "Obesidade"
    else:
        obs = "Normal"
    return obs

# %%
IMC(25)

# %%
#Importação da base de dados do Kaggle - Olympic Data
#lnk https://www.kaggle.com/datasets/bhanupratapbiswas/olympic-data?resource=download
dataset = pd.read_csv('dataset_olympics.csv')

# %%
#Visualizando o dataset
print(dataset)

# %%
#Retirando os atletas que não possuem informação de peso e altura
dataset_ajust_peso_alt = dataset.dropna(subset=['Height', 'Height'])

# %%
#Visualizando o dataset com apenas os atletas que possuem informação de peso e altura
print(dataset_ajust_peso_alt)

# %%
# O dataset otiginal possui 70000 dados o ajustado possui 53746 dados. Ou seja, o número abaixo representa a quant de atletas
#sem peso e altura
# 70000-53746

# %%
#Verificando a média de altura
dataset_ajust_peso_alt['Height'].mean()


# %%
#Verificando a média de peso
dataset_ajust_peso_alt['Weight'].mean()

# %%
#Ajustando a coluna de Altura para colocar em metros 
dataset_ajust_peso_alt['Height'] = dataset_ajust_peso_alt['Height']/100
#Criando uma nova coluna para cálculo do IMC
dataset_ajust_peso_alt['IMC'] = dataset_ajust_peso_alt['Weight']/(dataset_ajust_peso_alt['Height']*dataset_ajust_peso_alt['Height'])

# %%
print(dataset_ajust_peso_alt)

# %%
#Utilização da função apply para passar a função IMC.
#A função IMC diz se a pessoa está no estado de Magreza, Normal ou Obsidade
dataset_ajust_peso_alt['IMC - textual'] = dataset_ajust_peso_alt['IMC'].apply(IMC)

# %%
#Renomeando as colunas Height e Weight para Altura e Peso, respectivamente
dataset.rename(columns={'Height':'Altura', 'Weight': 'Peso'}, inplace = True)

# %%
#Visalizando o dataset com as colunas renomeadas
print(dataset)

# %%
#Realizando uma query/ filtro para pegar apenas os atletas com IMC - Textual = Magreza
print(dataset_ajust_peso_alt[dataset_ajust_peso_alt['IMC - textual'] == "Magreza"])

# %%
#Realizando uma query/ filtro para pegar apenas os atletas com IMC - Textual = Obesidade
obesidade = dataset_ajust_peso_alt.where(dataset_ajust_peso_alt['IMC - textual'] == 'Obesidade')
#Verificando apenas com score obesidade
obesidade.dropna(subset=['IMC - textual'])

# %%
#Salvando um novo dataset
dataset_ajust_peso_alt.to_excel('novos_dados.xlsx')

# %%
#Verificando a idade mínima do atleta
dataset['Age'].min()

# %%
#Verificando a idade máxima do atleta
dataset['Age'].max()

# %%
#Mediana de idade por ano e Sexo
dataset.groupby(['Year', 'Sex'])['Age'].median()

# %%
#Média do peso por ano e Sexo
dataset.groupby(['Year', 'Sex'])['Weight'].mean()

# %%
#Quantos times por ano
dataset.groupby(['Year'])['Team'].count()

# %%
#Plotando a quantidade de teams por anor
dataset.groupby(['Year'])['Team'].count().plot(kind='barh')

# %%
#Utilização do cut para definir horizontes temporais
# import pandas as pd
dataset['Periodo temporal'] = pd.cut(dataset['Year'], bins = 3, labels = ['antigo', 'intermediário', 'mais recente'])

# %%



