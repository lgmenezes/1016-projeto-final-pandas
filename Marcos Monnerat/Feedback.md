**PRINCIPAL (1000XP)**

- [✅] Escolher uma base de dados da sua preferência, através de sites como Kaggle, UCI, etc (50XP).
- Cada um dos requisitos abaixo deve vir acompanhado de uma pergunta. Por exemplo, "quantos meninos que tiraram nota acima de 5 existem na turma?" (50XP);
- [✅] Utilizar as seguintes funções pelo menos uma vez:
  - fillna, drop ou dropna (100XP);
  - apply (100XP);
  - rename (100XP);
- [✅] Realizar manipulações aritméticas necessárias na base de dados (soma, multiplicação, divisão, etc.) (100XP);
- [✅] Filtrar dados que sejam relevantes (filtros, query ou where) (100XP);
- [✅] Criar duas novas colunas, que venham a partir de alguma estatística (mean, median, max, etc.) (200XP);
- [✅] Utilizar o groupby, gerando alguma constatação estatística interessante (100XP);
- [✅] Utilizar o pd.qcut ou o pd.cut (100XP);

**EXTRA (200XP)**

- [✅] Gerar um gráfico a partir de qualquer dataframe utilizado no programa (matplotlib) (100XP);
- [✅] Exportar um dataframe para um CSV, desde que não seja igual ao original (100XP).

Comentários:

Trabalho perfeito, Marcos. Parabéns!

Cuidado, pois alguns comentários não indicavam o que estava sendo feito no código corretamente (-25XP).

De resto, tudo certinho!

**NOTA FINAL: 1175XP**
